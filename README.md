The System Design Project is a Semester 2 module and is a group project involving construction of an item of significant complexity under conditions designed to give insights into industrial teamwork. This year's task is to use Lego and an Arduino to design an assistive robotic device, with an appropriate software interface.

Our robot plays both reaction and memory games, can be controlled both via an App and by voice commands and reports results to an online server.

This repo contains the code snippets I wrote which are concerned with the movements of the robot, as well as with the mechanism of getting input from the user in the context of a memory game. 

The helpers module contains a suite of helper functions which are used to move some motors simultaneously to certain positions.
This module is imported in both the wheels module (relevant for the motors which operate the wheels of the robot) and the game1 module (which deals with all the gaming mechanics).